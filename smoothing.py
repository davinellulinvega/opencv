#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

# Import an image
img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.jpg')))

# 2D Convolution
# kernel = np.ones((5,5), np.float32) / 25  # Averaging
# kernel = np.array([[1,0,-1],[1,0,-1],[1,0,-1]], dtype=np.float32)  # Vertical edge
# kernel = np.array([[1,1,1],[0,0,0],[-1,-1,-1]], dtype=np.float32)  # Horizontal edge
# kernel = np.array([[1,0,-1], [2,0,-2],[1,0,-1]], dtype=np.float32)  # Sobel
kernel = np.array([[3,0,-3], [10,0,-10],[3,0,-3]], dtype=np.float32)  # Scharr
res = cv.filter2D(img, -1, kernel)

# Blurring
# Average
res = cv.blur(img, (5,5))
# res = cv.boxFilter(img, -1, (5,5))  # Achieves the same result as above, but more flexible
# Gaussian
res = cv.GaussianBlur(img, (5,5), 0)  # /!\ width and height of filter should be odd
# Median
res = cv.medianBlur(img, 5)  # /!\ Kernel size should be odd
# Bilateral filtering
res = cv.bilateralFilter(img, 9, 75, 75)

plt.subplot(1,2,1)
plt.imshow(img)
plt.title('Original')
plt.xticks([])
plt.yticks([])

plt.subplot(1,2,2)
plt.imshow(res)
plt.title('Result')
plt.xticks([])
plt.yticks([])
plt.show()

try:
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (5,5))  # Shape, size
    video = cv.VideoCapture(0)

    while True:
        alive, frame = video.read()
        if not alive:
            break
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        denoise = cv.morphologyEx(gray, cv.MORPH_OPEN, kernel)
        edges = cv.Sobel(denoise, cv.CV_64F, 1, 1, ksize=3)
        cv.imshow('Original',frame)
        cv.imshow('Edges',edges)
        if cv.waitKey(1) == ord('q'):
            break
finally:
    cv.destroyAllWindows()
    video.release()
