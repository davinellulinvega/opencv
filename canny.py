#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

canny = cv.cuda.createCannyEdgeDetector(75, 125, L2gradient=True)
gpu_img = cv.cuda_GpuMat()

try:
    # Import a random image
    img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')), cv.IMREAD_GRAYSCALE)
    gpu_img.upload(img)
    edges = canny.detect(gpu_img).download()

    # Display the edges
    cv.imshow('Original', img)
    cv.imshow('Edges', edges)
    while cv.waitKey(0) != ord('q'):
        pass

finally:
    cv.destroyAllWindows()

try:
    # Instantiate a video capture object
    video = cv.VideoCapture(0)

    while True:
        # Get the next frame
        alive, frame = video.read()
        if not alive:
            break

        # Canny edge detection
        frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        gpu_img.upload(frame)
        edges = canny.detect(gpu_img).download()

        # Display the original image and the edges
        cv.imshow('Original', frame)
        cv.imshow('Edges', edges)
        if cv.waitKey(1) == ord('q'):
            break

finally:
    cv.destroyAllWindows()
    video.release()
