#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

# Define some parameters required for yolo
CONF_THRESHOLD = 0.5
NMS_THRESHOLD = 0.4
IMG_WIDTH = 704
IMG_HEIGHT = 704
#IMG_WIDTH = 416
#IMG_HEIGHT = 416

# Load the classes' names
CLASSES_FILE = 'openimages.names'
classes = None
with open(CLASSES_FILE, 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

# Instantiate and configure the network
MODEL_CONF_FILE = 'yolo.cfg'
MODEL_WEIGHT_FILE = 'yolo.weights'

def getOutputsNames(net):
    # Get the name of all the layers in the network
    layers_names = net.getLayerNames()
    # Get the name of the output layers
    return [layers_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]


def postprocess(frame, outs):
    frame_h, frame_w = frame.shape[0:2]

    outs = np.vstack(outs)
    confidences = np.max(outs[:,5:], axis=1)
    outs = outs[np.flatnonzero(confidences > CONF_THRESHOLD),:]
    if outs.size == 0:
        return

    class_ids = np.argmax(outs[:,5:], axis=-1)
    confidences = np.max(outs[:,5:], axis=-1)

    # Compute the centers and the size of the bounding boxes
    # center_x, center_y, width, height all relative to frame size
    outs[:,0:4] *= np.array([frame_w, frame_h, frame_w, frame_h])
    center = outs[:, 0:2]
    size = outs[:, 2:4]
    left_top = center - size/2
    boxes = np.hstack((left_top, size)).astype(np.intp).tolist()

    # Non Maximum suppression
    indices = cv.dnn.NMSBoxes(boxes, confidences, CONF_THRESHOLD, NMS_THRESHOLD)
    for i in indices.flatten():
        left, top, width, height = boxes[i]
        drawPred(frame, class_ids[i], confidences[i], left, top, left+width, top+height)


def drawPred(frame, class_id, conf, left, top, right, bottom):
    cv.rectangle(frame, (left, top), (right, bottom), (255, 178, 50), 3)
    label = '{:.2f}'.format(conf)

    if classes:
        assert(class_id < len(classes))
        label = '{}: {}'.format(classes[class_id], label)

    label_size, baseline = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    top = max(top, label_size[1])
    cv.rectangle(frame, (left, top - round(1.5 * label_size[1])), (left + round(1.5*label_size[0]), top + baseline), (255, 255, 255), cv.FILLED)
    cv.putText(frame, label, (left, top), cv.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,0))


if __name__ == "__main__":
    net = cv.dnn.readNetFromDarknet(MODEL_CONF_FILE, MODEL_WEIGHT_FILE)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA)

    output_layers = getOutputsNames(net)

    # Load the input image
    video = cv.VideoCapture(0)
    # img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')))
    cpt = 0
    try:
        while video.isOpened():

            # Read the next frame
            alive, frame = video.read()
            if not alive:
                break

            # Only perform detection every 5 frames
            if cpt % 5 == 0:
                cpt = 0
                # Create a blob from the image
                blob = cv.dnn.blobFromImage(frame, 1/255, (IMG_WIDTH, IMG_HEIGHT), [0,0,0], 1, crop=False)

                # Set the blob as the network's input
                net.setInput(blob)

                # Run the forward pass to get a prediction
                outs = net.forward(output_layers)

                # Put some efficiency information on the image
                #t, _ = net.getPerfProfile()
                #label = 'Inference time: {:.2f} ms'.format(t * 1000 / cv.getTickFrequency())
                #cv.putText(frame, label, (0,15), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

            # Remove the bounding boxes with low confidence
            postprocess(frame, outs)

            # Display the image
            cv.imshow('Result', frame)
            if cv.waitKey(1) == ord('q'):
                break
            cpt += 1
    finally:
        video.release()
        cv.destroyAllWindows()
