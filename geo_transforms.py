#!/usr/bin/env python
import numpy as np
import cv2 as cv

# Load the image
img = cv.imread('i1.jpg')

# Extract the size
height, width, _ = img.shape

# Resize
resize = cv.resize(img, None, fx=2, fy=2, interpolation=cv.INTER_AREA)

# Translation
M = np.float32([[1,0,100], [0,1,50]])  # Matrix for a translation of (100, 50)
trans = cv.warpAffine(img, M, (width, height))

# Rotation
# Get the matrix corresponding to a 90 degrees rotation without scaling
# and with the center matching with the image's center
M = cv.getRotationMatrix2D(((width-1)/2, (height-1)/2), 90, 1)  # (center_x, center_y), degree, scale
rot = cv.warpAffine(img, M, (width, height))

try:
    cv.imshow('d', rot)
    while cv.waitKey(0) != ord('q'):
        pass
finally:
    cv.destroyAllWindows()
