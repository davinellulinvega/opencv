#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

# Import the image and convert it to grayscale (required)
img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.jpg')), cv.IMREAD_GRAYSCALE)

# Simple thresholding
_, thres_bin = cv.threshold(img, 127, 255, cv.THRESH_BINARY)
_, thres_bin_inv = cv.threshold(img, 127, 255, cv.THRESH_BINARY_INV)
_, thres_trunc = cv.threshold(img, 127, 255, cv.THRESH_TRUNC)
_, thres_tozero = cv.threshold(img, 127, 255, cv.THRESH_TOZERO)
_, thres_tozero_inv = cv.threshold(img, 127, 255, cv.THRESH_TOZERO_INV)

# Display the results in a plot for convenience
titles = ['Original image', 'Binary', 'Binary inv', 'Trunc', 'Tozero', 'Tozero inv']
images = [img, thres_bin, thres_bin_inv, thres_trunc, thres_tozero, thres_tozero_inv]

for i,(ti,im) in enumerate(zip(titles, images)):
    plt.subplot(2,3,i+1)
    plt.imshow(im, 'gray', vmin=0, vmax=255)
    plt.title(ti)
    plt.xticks([])
    plt.yticks([])
plt.show()

# Adaptive thresholding
img_b = cv.medianBlur(img, 5)  # This acts as denoising for the final image

# This is still binary, here only for comparison with the rest
_, th1 = cv.threshold(img_b, 127, 255, cv.THRESH_BINARY)

# This is finally the adaptive stuff
# /!\ The blocksize parameter has to be an odd number
th2 = cv.adaptiveThreshold(img_b, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 11, 2)
th3 = cv.adaptiveThreshold(img_b, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)


titles = ['Original', 'Global (v = 127)', 'Adaptive Mean', 'Adaptive Gaussian']
images = [img_b, th1, th2, th3]

for i, (ti, im) in enumerate(zip(titles, images)):
    plt.subplot(2,2,i+1)
    plt.imshow(im, 'gray')
    plt.title(ti)
    plt.xticks([])
    plt.yticks([])
plt.show()

# Otsu's binarization
# Again this is simple binary thresholding for comparison
_, th1 = cv.threshold(img, 127, 255, cv.THRESH_BINARY)

# Now that's Otsu
_, th2 = cv.threshold(img, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)

# Otsu with Gaussian filtering
blur = cv.GaussianBlur(img, (5,5), 0)
_, th3 = cv.threshold(blur, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)

# Display the image along with their respective histograms
images = [img, 0, th1, img, 0, th2, blur, 0, th3]
titles = ['Original', 'Histogram', 'Global thresh (v = 127)',
          'Original', 'Histogram', "Otsu's thresh",
          'Gaussian filtered', 'Histogram', "Otsu's thresh"]

for i in range(3):
    plt.subplot(3,3,i*3+1)
    plt.imshow(images[i*3], 'gray')
    plt.title(titles[i*3])
    plt.xticks([])
    plt.yticks([])

    plt.subplot(3,3,i*3+2)
    plt.hist(images[i*3].ravel(), 256)
    plt.title(titles[i*3+1])
    plt.xticks([])
    plt.yticks([])

    plt.subplot(3,3,i*3+3)
    plt.imshow(images[i*3+2], 'gray')
    plt.title(titles[i*3+2])
    plt.xticks([])
    plt.yticks([])

plt.show()
