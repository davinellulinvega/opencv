#!/usr/bin/python
# -*- coding: utf-8 -*-
# TODO: In general the idea is here, now you have to tidy up the code,
#  make it more robust, ensure that all parameters work in most environments,
#  and above all write comments
from os import uname
from argparse import ArgumentParser
import numpy as np
import cv2 as cv
from multi_tracking import MultiTracker, ObjCat, TrackType

BORDER_SIZE = 35

if __name__ == "__main__":
    # Create command line flags to enable/disable some features
    parser = ArgumentParser()
    parser.add_argument('--path', action='store_true', dest='path', help='Open a separate window to display the path traced by the tip of the finger.')
    parser.add_argument('--contour', action='store_true', dest='contour', help='Display the contour found in all the hand bound boxes.')
    parser.add_argument('--tip', action='store_true', dest='tip', help='Display a colored disk indicating the detected tip of the finger.')
    parser.add_argument('--main', action='store_true', dest='main', help='Open a separate window to display the original video stream.')

    # Parse command line arguments
    args = parser.parse_args()

    # Detect the type of process running the program
    machine = uname().machine
    # Initialize the video capturing device and the multi-tracker based on
    # the processor type
    if machine == 'aarch64' or machine.startswith('arm'):
        video = cv.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=3264, height=2464, format=(string)NV12, framerate=21/1 ! nvvidconv flip-method=2 ! video/x-raw, width=800, height=480, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv.CAP_GSTREAMER)

        multi_tracker = MultiTracker(TrackType.MOSSE, skip_frames=5, img_w=480, img_h=480, obj_cats=[ObjCat.HAND])
    else:
        video = cv.VideoCapture(0)
        multi_tracker = MultiTracker(TrackType.KCF, skip_frames=5, obj_cats=[ObjCat.HAND])

    # Make sure we have access to the video capturing device
    if not video.isOpened():
        print('Could not open the video capturing device.')
        exit(-1)

    # TODO: Check if this kernel is not too aggressive during the day or if its
    #  needs some adjusting
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (10, 10))  # Shape, size
    if args.path:
        path = np.zeros((int(video.get(cv.CAP_PROP_FRAME_HEIGHT)), int(video.get(cv.CAP_PROP_FRAME_WIDTH)), 3), dtype=np.uint8)
    # Start processing frames
    try:
        while True:
            # Read the next frame
            ok, frame = video.read()
            if not ok:
                print('No more frames to process. End of the stream?')
                break

            # Step though the tracking sequence
            multi_tracker.step(frame)

            # Detect edges inside the bounding boxes
            for obj_id, bbox in multi_tracker.bboxes.items():
                color = multi_tracker.colors[obj_id]
                y_min = int(max(0, bbox[1] - 2 * BORDER_SIZE))
                y_max = int(min(frame.shape[0], bbox[1] + bbox[3]))  # min(frame.shape[0], bbox[1] + bbox[3] + BORDER_SIZE)
                x_min = int(max(0, bbox[0]))  # max(0, bbox[0] - BORDER_SIZE)
                x_max = int(min(frame.shape[1], bbox[0] + bbox[2]))  #min(frame.shape[1], bbox[0] + bbox[2] + BORDER_SIZE)

                img_patch = frame[y_min:y_max, x_min:x_max]

                # TODO: Everything that comes after this only sort of work, so find another solution to find the pointer finger
                #    Maybe remove the background using histograms? Remove/reduce the border added on top?
                hsv_patch = cv.cvtColor(img_patch, cv.COLOR_BGR2HSV)
                yuv_patch = cv.cvtColor(img_patch, cv.COLOR_BGR2YUV)

                _, thres_patch = cv.threshold(yuv_patch[:, :, 0], 150, 255,
                                              cv.THRESH_BINARY)
                cv.imshow('Thresh', thres_patch)

                open_patch = cv.morphologyEx(thres_patch, cv.MORPH_OPEN, kernel)

                contours, _ = cv.findContours(open_patch, cv.RETR_EXTERNAL,
                                              cv.CHAIN_APPROX_SIMPLE)

                if args.main and args.contour:
                    cv.drawContours(img_patch, contours, -1, (0, 255, 0), thickness=2)

                if contours:
                    top_mosts = [tuple(cnt[cnt[:, :, 1].argmin()][0]) for cnt in contours]
                    top_most = sorted(top_mosts, key=lambda p: p[1])[0]

                # Draw the finger tip
                if args.main and args.tip:
                    cv.circle(img_patch, top_most, 10, color, thickness=-1)


                # Draw the path followed by the finger so far
                if args.path:
                    cv.circle(path, (top_most[0] + x_min, top_most[1]+y_min), 5, color, thickness=-1)

            # Display the resulting frame
            if args.main:
                cv.imshow('Multi-Tracker', frame)
            if args.path:
                cv.imshow('path', path)

            # Check if the use wants to quit
            if cv.waitKey(1) == ord('q'):
                break
    finally:
        video.release()
        cv.destroyAllWindows()
