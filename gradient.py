#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

# Import a random image
img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')), cv.IMREAD_GRAYSCALE)

# Laplace
laplacian = cv.Laplacian(img, cv.CV_64F, ksize=3)
# Sobel
sobel_x = cv.Sobel(img, cv.CV_64F, 1, 0, ksize=3)
sobel_y = cv.Sobel(img, cv.CV_64F, 0, 1, ksize=3)

# Scharr
scharr_x = cv.Scharr(img, cv.CV_64F, 1, 0)
scharr_y = cv.Scharr(img, cv.CV_64F, 0, 1)

# Display all images
cv.imshow('Original', img)
cv.imshow('Laplace', laplacian)
cv.imshow('Sobel X', sobel_x)
cv.imshow('Sobel Y', sobel_y)
cv.imshow('Scharr X', scharr_x)
cv.imshow('Scharr Y', scharr_y)
while cv.waitKey(0) != ord('q'):
    pass
cv.destroyAllWindows()

