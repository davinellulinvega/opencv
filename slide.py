#!/usr/bin/env python
import numpy as np
import cv2 as cv
from itertools import cycle
from more_itertools import windowed
from glob import glob
from random import choices

try:
    images = [cv.imread(p) for p in choices(glob('/home/davinellulinvega/Pictures/*.jpg'), k=20)]
    # h, w, _ = np.max([img.shape for img in images], axis=0)

    for o_img, n_img in windowed(cycle(images), 2):
        # o_h, o_w, _ = o_img.shape
        # n_h, n_w, _ = n_img.shape

        # o_img = cv.copyMakeBorder(o_img, (h - o_h) // 2, (h - o_h) // 2, (w - o_w) // 2, (w - o_w) // 2, cv.BORDER_CONSTANT, value=(0,0,0))
        # n_img = cv.copyMakeBorder(n_img, (h - n_h) // 2, (h - n_h) // 2, (w - n_w) // 2, (w - n_w) // 2, cv.BORDER_CONSTANT, value=(0,0,0))

        # m_h, m_w, c = np.min([o_img.shape, n_img.shape], axis=0)
        # o_img.resize((m_h,m_w,c))
        # n_img.resize((m_h,m_w,c))

        # Resize both images so they are of the same size, otherwise the cv.addWeighted() method will not work
        o_img = cv.resize(o_img, (800, 800), interpolation=cv.INTER_AREA)
        n_img = cv.resize(n_img, (800, 800), interpolation=cv.INTER_AREA)

        # Display the actual picture for 2s
        cv.imshow('Slideshow', o_img)
        if cv.waitKey(2000) == ord('q'):
            break

        # Fade in transition between the two pictures
        for i in range(10):
            trans_img = cv.addWeighted(o_img, 1-0.1*i, n_img, 0.1 * i, 0)
            cv.imshow('Slideshow', trans_img)
            cv.waitKey(100)
finally:
    # Clear the whole thing
    cv.destroyAllWindows()
