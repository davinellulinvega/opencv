#!/usr/bin/env python
from random import choice
from operator import itemgetter
from glob import glob
import numpy as np
import cv2 as cv

try:
    # Import a random image
    img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')))
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    # Find the edges with a canny detector
    edges = cv.Canny(gray, 75, 125)

    # Find the contours based on the edges
    contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_TC89_KCOS)

    # Contour approximation
    ca = []
    for cnt in contours:
        ca.append(cv.approxPolyDP(cnt, 0.001*cv.arcLength(cnt, True), True))

    # Convex hull
    hulls = []
    for cnt in contours:
        hulls.append(cv.convexHull(cnt, returnPoints=True))

    # Bounding rectangle
    rects = img.copy()
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        cv.rectangle(rects, (x,y), ((x+w), (y+h)), (0,255,0), 1)

    # Minimum bounding rectangle
    min_boxes = []
    for cnt in contours:
        rect = cv.minAreaRect(cnt)
        box = cv.boxPoints(rect).astype(np.int0)
        min_boxes.append(box)

    # Bounding circles
    circles = [cv.minEnclosingCircle(cnt) for cnt in contours]

    circles.sort(key=itemgetter(1), reverse=True)
    circles_img = img.copy()
    for (x,y), radius in circles:
        if radius < 100:
            break
        center = (int(x), int(y))
        radius = int(radius)
        cv.circle(circles_img, center, radius, (0,255,0), 1)

    # Bounding Ellipses
    ellipses = img.copy()
    for cnt in contours:
        if len(cnt) > 5:
            ellipse = cv.fitEllipse(cnt)
            cv.ellipse(ellipses, ellipse, (0,255,0), 1)

    # Draw all the contours on the original images
    min_rects = cv.drawContours(img.copy(), min_boxes, -1, (0, 255, 0), thickness=1)
    hull = cv.drawContours(img.copy(), hulls, -1, (0, 255, 0), thickness=1)
    approx = cv.drawContours(img.copy(), ca, -1, (0, 255, 0), thickness=1)
    res = cv.drawContours(img.copy(), contours, -1, (0, 255, 0), thickness=1)

    # Display the result
    # cv.imshow('Ellipses', ellipses)
    cv.imshow('Circles', circles_img)
    # cv.imshow('Rectangles', rects)
    # cv.imshow('Minimum Rectangles', min_rects)
    # cv.imshow('Hull', hull)
    # cv.imshow('Approx', approx)
    # cv.imshow('Contours', res)
    while cv.waitKey(0) != ord('q'):
        pass

finally:
    cv.destroyAllWindows()
