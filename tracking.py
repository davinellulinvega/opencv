#!/usr/bin/env python3
from os import uname
from random import randint
import numpy as np
import cv2 as cv

MACHINE = uname().machine

# Define the video capturing device
if  MACHINE == 'aarch64' or MACHINE.startswith('arm'):
    GSTREAMER_PIPELINE = 'nvarguscamerasrc ! video/x-raw(memory:NVMM), width=3264, height=2464, format=(string)NV12, framerate=21/1 ! nvvidconv flip-method=2 ! video/x-raw, width=960, height=616, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink'
    video = cv.VideoCapture(GSTREAMER_PIPELINE, cv.CAP_GSTREAMER)
else:
    video = cv.VideoCapture(0)

# Make sure the webcam is available
if not video.isOpened():
    print('Could not open the video capturing device.')
    exit(-1)

# Read the first frame
ok, frame = video.read()
if not ok:
    print('Could not read the first frame.')
    video.release()
    exit(-1)

try:
    # Select the region of interest
    bbox = cv.selectROI('tracker', frame, False)
    color = (randint(0,255), randint(0,255), randint(0,255))

    # Instantiate and initialize the tracker
    if  MACHINE == 'aarch64' or MACHINE.startswith('arm'):
        # Simple, but faster. Therefore, adapted to edge devices
        tracker = cv.legacy.TrackerMOSSE_create()
    else:
        # More accurate, but slower. Therefore, adapted for 'real' CPUs
        #tracker = cv.TrackerKCF_create()
        tracker = cv.TrackerCSRT_create()
    tracker.init(frame, bbox)

    # Track the object across frames
    while True:
        # Read the next frame
        ok, frame = video.read()
        if not ok:
            print('End of the video stream')
            break

        # Update the bounding box
        # It should be noted that the bounding box is a cv::Rect object
        # whose parameters are: X, Y coordinates of top-left corner, width and height of rectangle
        ok, bbox = tracker.update(frame)

        # If the object has not been lost draw its bounding box
        if ok:
            tl = (int(bbox[0]), int(bbox[1]))
            br = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv.rectangle(frame, tl, br, color, 2, 1)

        # Otherwise exit
        else:
            break

        # Display the frame
        # Note that we are using the same window name as the selectROI() function
        # so that the same window is reused and we do not need to destroy it
        # in-between
        cv.imshow('tracker', frame)

        # Give the user the option to quit
        if cv.waitKey(1) == ord('q'):
            break

finally:
    cv.destroyAllWindows()
    video.release()
