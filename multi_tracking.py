#!/usr/bin/python
# -*- coding: utf-8 -*-
from os import uname
from random import randint
from enum import IntEnum
from itertools import chain
from queue import Queue, Empty
from collections import deque
from threading import Thread
import numpy as np
import cv2 as cv
# Make sure OpenCV uses optimized code
cv.setUseOptimized(True)


class ObjCat(IntEnum):
    HAND = 0
    HEAD = 1


class TrackType(IntEnum):
    CSRT = 0
    KCF = 1
    MOSSE = 2


class ObjectDetectionNTracking(Thread):
    """
    A simple class that uses both an ObjectDetector to detect objects in a frame and multiple ObjectTrackers to track
    detected objects across frames.
    """

    def __init__(self, capture_device, tracker_type, skip_frames,
                 obj_cats, conf_thres=0.5, nms_thres=0.3, conf_decay_rate=0.998,
                 img_w=704,
                 img_h=704, classes_name_file='openimages.names',
                 model_conf_file='yolo.cfg', model_weight_file='yolo.weights'):
        """
        Initialize the different attributes required to detect and track objects in pictures.
        :param capture_device: cv.VideoCapture. An object representing the device from which to capture new frame.
        :param tracker_type: TrackType. The type of tracker to use. Some are more precise than others, but take more
        CPU/GPU power to run.
        :param skip_frames: Int. The number of frames to skip in-between two detection event. Since running YoloV4 might
        take quite a lot of GPU power it can be interesting for low-spec computers to only run the detection algorithm
        every so often.
        :param obj_cats: List. A list of categories of objects to detect and track in every picture.
        :param conf_thres: Float. The confidence threshold above which an object is considered as detected.
        :param nms_thres: Float. The non-maximum threshold which determines how dissimilar two objects should be to be
        considered as separate.
        :param conf_decay_rate: Float. The rate at which the confidence associated with each object decays. This prevent
        the tracker from drifting too much by virtually making objects disappear and forcing the detector to find the
        object again.
        :param img_w: Int. The width of the frame passed to the detector network. This size should be a multiple of 32.
        :param img_h: Int. The height of the frame passed to the detector network. This size should be a multiple of 32.
        :param classes_name_file: String. The relative path to the text file containing the name of the categories of
        object to be detected.
        :param model_conf_file: String. The relative path to the file containing the configuration of the YoloV4 network
        to be used as the object detector.
        :param model_weight_file: String. The relative path to the file containing the pre-trained weights for the
        detector network.
        """

        # Initialize the parent class
        super(ObjectDetectionNTracking, self).__init__()

        # Capture device
        self._video_capture = capture_device

        # How many frames to skip between detection
        assert skip_frames > 0, "The number of frames skipped between " \
                                "detection event should be greater than or " \
                                "equal to 1."
        self._skip_frames = skip_frames
        self._frame_cpt = 0

        # Get an object tracker
        self._tracker = ObjectTracker(tracker_type, conf_thres, nms_thres, conf_decay_rate)

        # Get an object detector
        self._detector = ObjectDetector(obj_cats, conf_thres, nms_thres, img_w, img_h, classes_name_file,
                                        model_conf_file, model_weight_file)

        # Add the tracker's update_trakers() function as a listener to the detector
        self._detector.add_listener(self._tracker.update_trackers)

        # Start both the tracker and detector
        self._tracker.start()
        self._detector.start()

        # Declare a flag telling the thread when to stop
        self._continue = True

    def run(self):
        """
        Capture next frame from video device and hand it to both the ObjectTracker, and ObjectDetector 
        (when appropriate).
        :return: None
        """
        while self._continue:
            # Read the next frame
            ok, frame = self._video_capture.read()

            # If nothing was captured by the video device, then exit the loop
            if not ok:
                print('No more frames to process. End of the stream?')
                self._continue = False
                continue

            # Pass the new frame to the tracker
            self._tracker.track_objects(frame)

            # On the very first frame, or if no object is currently tracked,
            # or on the nth frame
            # /!\ Never reinitialize the number of skipped frames
            if self._frame_cpt == 0 or self._tracker.nb_tracked_objects == 0 or \
                    self._frame_cpt % self._skip_frames == 0:
                # Pass the new frame to the detector
                self._detector.detect_objects(frame)

            # Increase the number of frames processed
            self._frame_cpt += 1

    def get_next_frame(self, block=False):
        """
        Get the next processed from from the ObjectTracker directly.
        This method is not really required and adds layers to an already high cake,
        but it hides away the 'complexities' of the implementation.
        :return: None
        """
        # Return next processed frame from the tracker
        return self._tracker.get_next_frame(block=block)

    def stop(self):
        """
        Cleanup before destroying the instance.
        :return: None
        """

        # Tell the thread to stop
        self._continue = False

        # Release the video capture device
        self._video_capture.release()

        # Ask the tracker to stop
        self._tracker.stop()
        self._tracker.join()

        # Ask the detector to stop
        self._detector.stop()
        self._detector.join()

    @property
    def done(self):
        return not self._continue


class ObjectTracker(Thread):
    """
    A class that uses multiple trackers to follow detected objects across frames.
    """

    def __init__(self, tracker_type, conf_thres=0.5, nms_thres=0.3, conf_decay_rate=0.998):
        """
        Initialize the different attributes required to track objects in pictures.
        :param tracker_type: TrackType. The type of tracker to use. Some are more precise than others, but take more
        CPU/GPU power to run.
        :param conf_thres: Float. The confidence threshold above which an object is considered as detected.
        :param nms_thres: Float. The non-maximum threshold which determines how dissimilar two objects should be to be
        considered as separate.
        :param conf_decay_rate: Float. The rate at which the confidence associated with each object decays. This prevent
        the tracker from drifting too much by virtually making objects disappear and forcing the detector to find the
        object again.
        """

        # Initialize the parent class
        super(ObjectTracker, self).__init__()

        # Tracker, colors, and bonding box management
        self._tracker_type = tracker_type
        self._trackers = {}
        self._colors = {}
        self._bboxes = {}
        self._confidences = {}
        self._next_id = 0

        # Other miscellaneous parameters
        self._conf_decay_rate = conf_decay_rate
        self._conf_thres = conf_thres
        self._nms_thres = nms_thres

        # Initialize a queue in which the next frame to be processed will be put
        self._in_queue = Queue()

        # Initialize a queue in which processed images will be put
        self._out_queue = Queue()

        # Initialize a flag telling the thread when to stop
        self._continue = True

    def run(self):
        while self._continue:
            # Get the next frame from the input queue
            try:
                frame = self._in_queue.get(block=False)

                # Indicate to the input queue that the task has been done
                self._in_queue.task_done()
            except Empty:
                continue

            # Update the bounding boxes of all tracked objects
            dead = self._update(frame)
            # Remove dead objects
            if len(dead) != 0:
                self._remove_trackers(dead)

            # Draw the bounding boxes
            self._draw_bounding_boxes(frame)

            # Make sure the thread was not killed during processing
            if self._continue:
                # Put the processed frame in the output queue
                self._out_queue.put(frame)

    def stop(self):
        """
        Cleanup before destroying the instance.
        :return: None
        """

        # Tell the thread to stop
        self._continue = False

        # Empty the input queue
        print('T: Emptying input queue ...')
        while not self._in_queue.empty():
            self._in_queue.get()
            self._in_queue.task_done()
        # Stop the input queue
        self._in_queue.join()
        print('T: Done.')

        # Empty the output queue
        print('T: Emptying output queue ...')
        while not self._out_queue.empty():
            self._out_queue.get()
            self._out_queue.task_done()
        # Stop the output queue
        self._out_queue.join()
        print('T: Done.')

    def track_objects(self, frame):
        """
        This is a simple wrapper around a Queue.put() method for the input queue of the ObjectTracker.
        It is only here to hide some of the complexities of the implementation.
        The only thing done here is to put the frame given in parameter in the tracker's input queue.
        :param frame: Numpy.ndarray. An array representing the next frame in the video stream.
        :return: None
        """

        # Add the given frame to the input queue to be processed during the next cycle
        self._in_queue.put(frame)

    def get_next_frame(self, block=False):
        """
        Get the next processed frame from the output queue of the tracker.
        :param block: Boolean. If true the call to Queue.get() makes the program pause until a frame
        is available. Otherwise, a frame is returned only if it is present at the right time.
        If no frames are available and block is False, this method raises an Empty exception.
        :return: Numpy.ndarray
        """

        # Get the processed frame from the output queue
        frame = self._out_queue.get(block=block)
        # Indicate to the output queue that the task is done
        self._out_queue.task_done()

        # Return the processed frame
        return frame

    def update_trackers(self, bboxes, frame):
        """
        Given the bounding boxes and the frame in parameter update the list of trackers.
        Adding new ones for new objects. No tracker is being removed here this is actually done in the run() method.
        :param bboxes: dict. A dictionary whose keys are the categories of the objects detected, and the values are
        lists of coordinates representing the top-left corner of the bounding box, as well as its size.
        :param frame: Numpy.ndarray. An array corresponding to the frame in which the objects represented by the
        bounding boxes where detected. This is necessary for the trackers to initialize correctly.
        :return: None
        """

        if len(bboxes) != 0:
            # Add previously untracked objects
            for bbox, conf in chain(*bboxes.values()):
                # To check that the object is not already tracked,
                # we compute the IOU. If the iou is over a given
                # threshold, the two boxes are considered to be linked to
                # the same object. This is similar to the NMS algorithm,
                # but without taking the confidences into account.
                for tracked_bbox in self._bboxes.values():
                    # Compute the area of the intersection between the
                    # new object and the tracked object
                    inter_area = max(0, min(bbox[0] + bbox[2], tracked_bbox[0] + tracked_bbox[2]) - max(bbox[0],
                                                                                                        tracked_bbox[0])) * \
                                 max(0, min(bbox[1] + bbox[3], tracked_bbox[1] + tracked_bbox[3]) - max(bbox[1],
                                                                                                        tracked_bbox[1]))

                    # Compute the area of the new object
                    bbox_area = bbox[2] * bbox[3]
                    # Compute the area of the tracked object
                    tracked_bbox_area = tracked_bbox[2] * tracked_bbox[3]

                    # Finally compute the iou
                    iou = inter_area / (bbox_area + tracked_bbox_area - inter_area)
                    if iou > self._nms_thres:
                        break
                else:
                    # We did not find any similar tracked object,
                    # so this is a new one that should be tracked
                    self._add_trackers(frame, bbox, conf)

    def _draw_bounding_boxes(self, frame):
        # Draw the bounding boxes of all tracked objects
        for obj_id, bbox in self._bboxes.items():
            cv.rectangle(frame, (int(bbox[0]), int(bbox[1])),
                         (int(bbox[0]+bbox[2]), int(bbox[1]+bbox[3])),
                         self._colors[obj_id], thickness=2)

    def _add_trackers(self, frame, bbox, conf):
        # Instantiate and initialize a new tracker for each detected
        # object
        tracker = self._get_tracker()
        # The tuple conversion is required for python < 3.7
        tracker.init(frame, tuple(bbox))

        # Get a unique random color
        color = (randint(0, 255), randint(0, 255), randint(0, 255))
        while color in self._colors.values():
            color = (randint(0, 255), randint(0, 255), randint(0, 255))

        # Store everything related to the object into the
        # corresponding structure
        self._bboxes[self._next_id] = bbox
        self._trackers[self._next_id] = tracker
        self._colors[self._next_id] = color
        self._confidences[self._next_id] = conf

        # Increase the number of objects tracked so far
        self._next_id += 1

    def _remove_trackers(self, obj_ids):
        # Remove everything related to each object in the list
        for obj_id in obj_ids:
            self._trackers.pop(obj_id)
            self._colors.pop(obj_id)
            self._bboxes.pop(obj_id)
            self._confidences.pop(obj_id)

    def _update(self, frame):
        # Declare a list of all the trackers that failed
        dead = []

        # To prevent modifications from happening during processing
        tmp = list(self._trackers.items())

        # Update the bounding boxes of all tracked objects
        for obj_id, tracker in tmp:
            # By default remove any object whose confidence is not above
            # threshold anymore
            if self._confidences[obj_id] < self._conf_thres:
                dead.append(obj_id)
            else:
                # Slowly decay the confidence associated with the object
                self._confidences[obj_id] *= self._conf_decay_rate
                # Update the tracked object's bounding box
                ok, bbox = tracker.update(frame)
                if not ok:
                    # The tracker has failed
                    dead.append(obj_id)
                else:
                    self._bboxes[obj_id] = bbox

        # Return the list of failed trackers
        return dead

    def _get_tracker(self):
        if self._tracker_type == TrackType.MOSSE:
            return cv.legacy.TrackerMOSSE_create()
        if self._tracker_type == TrackType.KCF:
            return cv.TrackerKCF_create()
        if self._tracker_type == TrackType.CSRT:
            return cv.TrackerCSRT_create()

    @property
    def bboxes(self):
        return self._bboxes

    @property
    def confidences(self):
        return self._confidences

    @property
    def colors(self):
        return self._colors

    @property
    def nb_tracked_objects(self):
        return len(self._trackers)


class ObjectDetector(Thread):
    """
    A class that uses a Yolo V4 neural network to detect objects of different categories in a frame.
    """

    def __init__(self, obj_cats, conf_thres=0.5, nms_thres=0.3, img_w=704, img_h=704,
                 classes_name_file='openimages.names', model_conf_file='yolo.cfg', model_weight_file='yolo.weights'):
        """
        Initialize the different attributes required to detect objects in pictures.
        :param obj_cats: List. A list of categories of objects to detect and track in every picture.
        :param conf_thres: Float. The confidence threshold above which an object is considered as detected.
        :param nms_thres: Float. The non-maximum threshold which determines how dissimilar two objects should be to be
        considered as separate.
        :param img_w: Int. The width of the frame passed to the detector network. This size should be a multiple of 32.
        :param img_h: Int. The height of the frame passed to the detector network. This size should be a multiple of 32.
        :param classes_name_file: String. The relative path to the text file containing the name of the categories of
        object to be detected.
        :param model_conf_file: String. The relative path to the file containing the configuration of the YoloV4 network
        to be used as the object detector.
        :param model_weight_file: String. The relative path to the file containing the pre-trained weights for the
        detector network.
        """

        # Call the constructor for the parent class
        super(ObjectDetector, self).__init__()

        # Classes and categories management
        assert hasattr(obj_cats, '__getitem__')
        self._tracked_cats = obj_cats
        self._avail_classes = []
        with open(classes_name_file, 'rt') as f:
            self._avail_classes = f.read().rstrip('\n').split('\n')

        # Yolo parameters
        self._conf_thres = conf_thres
        self._nms_thres = nms_thres
        self._img_size = (img_w, img_h)

        # Instantiate a yolo-based object detection network
        self._dnn = cv.dnn.readNetFromDarknet(model_conf_file,
                                              model_weight_file)
        self._dnn.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA)
        self._dnn.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA)
        # Get the name of the output layers
        self._out_layers_name = [self._dnn.getLayerNames()[i[0] - 1] for i in
                                 self._dnn.getUnconnectedOutLayers()]

        # Initialize a deque in which the next frame to be processed will be put
        # We limit the size of the deque to 1 since only the most recent frame is of interest to us
        self._in_queue = deque(maxlen=1)

        # Initialize a list of callable observers to notify when detecting objects
        self._observers = []

        # Define a flag telling the thread when to stop
        self._continue = True

    def add_listener(self, func):
        """
        Add a function to the list of observers for the availability of new bounding boxes.
        :param func: Callable. A callable that takes a dictionary of bounding boxes, and a frame
        as parameters.
        :return: None
        """

        # Make sure the given parameter is a callable
        if callable(func):
            # Add the listener to the list
            self._observers.append(func)
        else:
            print('The parameter {} provided should be a callable.'.format(func))

    def remove_listener(self, func):
        """
        Remove a function from the list of observers.
        :param func: Callable. A function previously added to the list of observers.
        :return: None
        """

        try:
            # Remove the corresponding listener from the list of observers
            self._observers.remove(func)
        except ValueError:
            print('{} was not found in the list of observers, so not removed.'.format(func))

    def clear_listeners(self):
        """
        Remove all the observers.
        :return: None
        """

        # Assign a new empty list to the observers attribute
        self._observers = []

    def run(self):
        while self._continue:
            try:
                # Get the next frame
                frame = self._in_queue.pop()
            except IndexError:
                continue

            # Detect objects and extract corresponding bboxes
            outs = self._detect(frame)
            frame_h, frame_w = frame.shape[0:2]
            bboxes = self._extract_bboxes(outs, frame_w, frame_h)

            # Make sure the thread was not killed during processing
            if self._continue and len(bboxes) != 0:
                # Notify the listeners of the new bounding boxes
                for obs in self._observers:
                    obs(bboxes, frame)

    def stop(self):
        """
        Cleanup before destroying the instance.
        :return: None
        """

        # Ask the thread to stop
        self._continue = False

        # Empty the input queue
        print('D: Emptying input queue ...')
        self._in_queue.clear()
        print('D: Done.')

    def detect_objects(self, frame):
        """
        A simple wrapper around the append() method of the detector's input deque.
        Therefore, it just adds the frame given in parameter to the deque.
        :param frame: Numpy.ndarray. An array representing a frame from the video stream, in which
        objects are to be detected.
        :return: None
        """

        # Add the given frame to the input queue so that it is processed during the next cycle
        self._in_queue.append(frame)

    def _detect(self, frame):
        # Create a blob from the image
        blob = cv.dnn.blobFromImage(frame, 1 / 255, (self._img_size[0],
                                                     self._img_size[1]),
                                    [0, 0, 0], 1, crop=False)

        # Set the blob as the network's input
        self._dnn.setInput(blob)

        # Run the forward pass to get a prediction
        return self._dnn.forward(self._out_layers_name)

    def _extract_bboxes(self, outs, frame_w, frame_h):
        # TODO: Add comment to describe what you are doing here
        outs = np.vstack(outs)
        confidences = np.max(outs[:, 5:], axis=1)
        outs = outs[np.flatnonzero(confidences > self._conf_thres), :]
        if outs.size == 0:
            return {}

        # Compute the centers and the size of the bounding boxes
        # center_x, center_y, width, height all relative to frame size
        outs[:, 0:4] *= np.array([frame_w, frame_h, frame_w, frame_h])
        center = outs[:, 0:2]
        size = outs[:, 2:4]
        left_top = center - size/2
        boxes = np.hstack((left_top, size)).astype(np.intp).tolist()

        # Non Maximum suppression
        confidences = np.max(outs[:, 5:], axis=-1)
        indices = cv.dnn.NMSBoxes(boxes, confidences, self._conf_thres,
                                  self._nms_thres)
        bboxes = {}
        class_ids = np.argmax(outs[:, 5:], axis=-1)
        for i in indices.flatten():
            cat = ObjCat(class_ids[i])
            if cat in self._tracked_cats:
                bboxes.setdefault(cat, []).append((boxes[i], confidences[i]))
        return bboxes


class Display(object):
    """
    This is just a wrapper around the imshow() method from the OpenCV library to make things more convenient.
    """

    def __init__(self, win_name):
        """
        Initialize the parameters required for displaying images.
        :param win_name: String. The name of the window in which the video stream will be displayed.
        """

        # Call the constructor for the parent class
        super(Display, self).__init__()

        # Store the name of the window
        self._win_name = win_name

        # Initialize a display thread
        cv.startWindowThread()
        # And a named window inside it
        cv.namedWindow(self._win_name)

    def step(self, frame):
        """
        Take the frame give in parameter and display it in the window.
        :param frame: Numpy.ndarray. A numpy array representing the next frame in the video stream.
        :return: None
        """

        # Do what it says on the tin
        cv.imshow(self._win_name, frame)

    def stop(self):
        """
        Destroy the named window created upon initialization.
        :return: None
        """

        # Do what it says on the tin
        cv.destroyWindow(self._win_name)


if __name__ == "__main__":
    machine = uname().machine
    # Initialize the video capturing device and the multi-tracker based on
    # the processor type
    if machine == 'aarch64' or machine.startswith('arm'):
        #video = cv.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=3264, height=2464, format=(string)NV12, framerate=21/1 ! nvvidconv flip-method=2 ! video/x-raw, width=960, height=616, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv.CAP_GSTREAMER)
        video = cv.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=1280, height=720, format=(string)NV12, framerate=21/1 ! nvvidconv flip-method=2 ! video/x-raw, width=960, height=616, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv.CAP_GSTREAMER)

        tracker_type = TrackType.MOSSE
    else:
        video = cv.VideoCapture(0)
        tracker_type = TrackType.KCF

    # Make sure we have access to the video capturing device
    if not video.isOpened():
        print('Could not open the video capturing device.')
        exit(-1)

    # Instantiate an objects detector and tracker
    detect_n_track = ObjectDetectionNTracking(video, tracker_type=tracker_type, skip_frames=5,
                                              obj_cats=[ObjCat.HAND, ObjCat.HEAD])

    # Instantiate a display
    display = Display(win_name='Tracker')

    # Start processing frames
    detect_n_track.start()

    # Start displaying frames
    try:
        while not detect_n_track.done:
            try:
                # Get the next frame from the detecting and tracking thread
                new_frame = detect_n_track.get_next_frame(block=False)
                # Display the resulting frame
                display.step(new_frame)

            except Empty:
                pass

            # Check if the use wants to quit
            if cv.waitKey(25) == ord('q'):
                break
    finally:
        # Stop the detection and tracking loop
        detect_n_track.stop()
        detect_n_track.join()

        # Stop the display
        display.stop()
