#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

try:
    # Import a random image
    img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')))
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    # Find the edges with a canny detector
    edges = cv.Canny(gray, 75, 125)

    # Find the contours based on the edges
    contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_TC89_KCOS)

    # Draw all the contours on the original images
    res = cv.drawContours(img, contours, -1, (0, 255, 0), thickness=1)

    # Display the result
    cv.imshow('Result', res)
    while cv.waitKey(0) != ord('q'):
        pass

finally:
    cv.destroyAllWindows()


# Instantiate a video capture object
video = cv.VideoCapture(0)

gpu_img = cv.cuda_GpuMat()
canny = cv.cuda.createCannyEdgeDetector(75, 125, L2gradient=True)

try:
    while True:
        # Get the next frame
        alive, frame = video.read()
        if not alive:
            break

        # Canny edge detection
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        gpu_img.upload(gray)
        edges = canny.detect(gpu_img).download()

        # Find the contours
        contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_TC89_KCOS)

        # Draw the contours on the original frame
        res = cv.drawContours(frame, contours, -1, (0, 255, 0), thickness=1)

        # Display the result
        cv.imshow('Contours', res)
        if cv.waitKey(1) == ord('q'):
            break

finally:
    cv.destroyAllWindows()
    video.release()
