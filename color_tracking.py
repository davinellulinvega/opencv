import numpy as np
import cv2 as cv

cap = cv.VideoCapture(0)

try:
    while True:
        # Get the next frame
        ret, frame = cap.read()

        # End of the stream
        if not ret:
            break

        # Convert from BGR to HSV
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)

        # Threshold the image to extract colored objects/areas
        lower_blue = np.array([110,50,50])
        upper_blue = np.array([130,255,255])
        mask_b = cv.inRange(hsv, lower_blue, upper_blue)

        lower_red = np.array([0,50,50])
        upper_red = np.array([10,255,255])
        mask_r = cv.inRange(hsv, lower_red, upper_red)

        lower_green = np.array([50,50,50])
        upper_green = np.array([70,255,255])
        mask_g = cv.inRange(hsv, lower_green, upper_green)

        # Apply the mask to the original image through a bitwise-AND operation
        # /!\ OpenCV does more that just: res = frame * mask[..., np.newaxis] 
        #     see the core functionalities tuto and the explanation on the 
        #     difference between np.add() and cv.add()
        res_b = cv.bitwise_and(frame, frame, mask=mask_b)
        res_g = cv.bitwise_and(frame, frame, mask=mask_g)
        res_r = cv.bitwise_and(frame, frame, mask=mask_r)

        # Combine the different results together
        res = cv.add(cv.add(res_b, res_g), res_r)

        # Display the original video, the masks, and the result in separate windows
        cv.imshow('frame', frame)
        cv.imshow('mask_b', mask_b)
        cv.imshow('mask_g', mask_g)
        cv.imshow('mask_r', mask_r)
        cv.imshow('result', res)

        if cv.waitKey(25) == ord('q'):
            break

finally:
    cap.release()
    cv.destroyAllWindows()
