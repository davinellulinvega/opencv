#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

try:
    # Import a random image
    img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')))

    # Go Up the pyramid to get a lower resolution image
    low_res = img.copy()
    for _ in range(2):
        low_res = cv.pyrDown(low_res)

    # Go back Down the pyramid to get a higher resolution image
    high_res = low_res.copy()
    for _ in range(2):
        high_res = cv.pyrUp(high_res)

    # Display the edges
    cv.imshow('Original', img)
    cv.imshow('Low Res', low_res)
    cv.imshow('High Res', high_res)
    while cv.waitKey(0) != ord('q'):
        pass

finally:
    cv.destroyAllWindows()

# Blending two images
try:
    # Import a random image
    im1 = cv.imread('i1.jpg')
    im2 = cv.imread('i2.jpg')

    # Generate the Gaussian pyramid for the first image
    g_1 = im1.copy()
    gp_1 = [g_1]

    for _ in range(6):
        g_1 = cv.pyrDown(g_1)
        gp_1.append(g_1)

    # Generate the Gaussian pyramid for the second image
    g_2 = im2.copy()
    gp_2 = [g_2]

    for _ in range(6):
        g_2 = cv.pyrDown(g_2)
        gp_2.append(g_2)

    # Generate the Laplacian for the first image
    lp_1 = [gp_1[-1]]
    for i in range(5,0,-1):
        ge = cv.pyrUp(gp_1[i])
        ge.resize(gp_1[i-1].shape)
        l = cv.subtract(gp_1[i-1], ge)
        lp_1.append(l)

    # Generate the Laplacian for the second image
    lp_2 = [gp_2[-1]]
    for i in range(5,0,-1):
        ge = cv.pyrUp(gp_2[i])
        ge.resize(gp_2[i-1].shape)
        l = cv.subtract(gp_2[i-1], ge)
        lp_2.append(l)

    # Add the left and right halves of the images in each level
    ls = []
    for l1, l2 in zip(lp_1,lp_2):
        _, cols, _ = l1.shape
        ls.append(np.hstack((l1[:,0:cols//2], l2[:,cols//2:])))

    # Reconstruct the blended image
    blend = ls[0].copy()
    for i in range(1,len(ls)):
        blend = cv.pyrUp(blend)
        blend = cv.add(blend, ls[i])

    # Display the blend image
    cv.imshow('Blend', blend)
    while cv.waitKey(0) != ord('q'):
        pass

finally:
    cv.destroyAllWindows()
