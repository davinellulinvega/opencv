#!/usr/bin/env python3
from random import choice
from glob import glob
import cv2 as cv

# Get random image
#img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')))
video = cv.VideoCapture(0)

if video.isOpened():
    # Instantiate a background subtraction algorithm
    back_sub = cv.createBackgroundSubtractorMOG2(detectShadows=False)

    try:
        while True:
            ok, frame = video.read()
            if not ok:
                break

            # Apply the subtraction algorithm to get a foreground mask
            mask = back_sub.apply(frame)

            mask = cv.merge((mask, mask, mask))
            frame = cv.bitwise_and(frame, mask)

            # Display image and mask
            cv.imshow('Display', frame)
            cv.imshow('Mask', mask)
            if cv.waitKey(1) == ord('q'):
                break

    finally:
        video.release()
        cv.destroyAllWindows()
