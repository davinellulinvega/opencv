#!/usr/bin/env python
from random import choice
from glob import glob
import numpy as np
import cv2 as cv

# Import a random image
img = cv.imread(choice(glob('/home/davinellulinvega/Pictures/*.*p*g')), cv.IMREAD_GRAYSCALE)

# Define the common kernel
kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5,5))  # Shape, size

if False:
# Erosion
    erosion = cv.erode(img, kernel, iterations=1)

# Dilation
    dilation = cv.dilate(img, kernel, iterations=1)

# Opening (= erosion + dilation, removes noise)
    opening = cv.morphologyEx(img, cv.MORPH_OPEN, kernel)

# Closing (= dilation + erosion, closing small holes/black points inside foreground objects)
    closing = cv.morphologyEx(img, cv.MORPH_CLOSE, kernel)

# Gradient (= dilation - erosion)
    gradient = cv.morphologyEx(img, cv.MORPH_GRADIENT, kernel)

# Top Hat (= img - opening)
    top_hat = cv.morphologyEx(img, cv.MORPH_TOPHAT, kernel)

# Black Hat (= closing - img)
black_hat = cv.morphologyEx(img, cv.MORPH_BLACKHAT, kernel)

cv.imshow('Original', img)
cv.imshow('Transformed', black_hat)
while cv.waitKey(0) != ord('q'):
    pass
cv.destroyAllWindows()
